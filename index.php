<!-- HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Canvas Hexagonal Map</title>
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
        <script src="jquery-1.11.1.min.js"></script>
        <script src="hexmap.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <div id="wrapper">
            <div class="container">
                <div id="newMap" class="buttonBar left" >
                <legend>GM Controls</legend>
                    <h4>Input data here to create a new map</h4>
                    <table>
                        <tr><td>New Map Name: </td><td><input type="text" id="mapName" /></td></tr>
                        <tr><td>Hex Height: </td><td><input type="number" id="height" onchange="DrawHexGrid()" value="50" /></td></tr>
                        <tr><td>Number of Columns: </td><td><input type="number" id="columnNum" onchange="DrawHexGrid()" value="10" /></td></tr>
                        <tr><td>Number of Rows: </td><td><input type="number" id="rowNum" onchange="DrawHexGrid()" value="10" /></td></tr>
                        <tr><td colspan="2"><button id="testGenMethod" onclick="GenerateMap()" style="float:right;">Generate New Map</td></tr></button>
                        <tr><td colspan="2">*Generates a map in the database that can be accessed with Display Map pane.</td></tr>
                    </table>
                </div>
                <div id="displayMap" class="buttonBar left" >
                <legend>Player Controls</legend>
                    <h4>Display Map</h4>
                    <table>
                        <tr><td>Select A Map To Display: </td><td><label id="mapSel" name="mapSel"><?php include 'MapSelect.php'; ?></label></td></tr>
                        <tr><td>Rescale Grid</td><td><input type="number" id="displayheight" onchange="RescaleGrid()" value="50" /></td></tr>
                    </table>
                </div>
                <div class="buttonBar left">
                <legend>Universal Controls</legend>
                    <button id="toggleCoords">Toggle Coordinates</button><br>
                    Zoom: - <input type="range" id="zoom" name="zoom" min="10" max="50" value="10" onchange="ZoomSlider(this)"> +<br>
                </div>
                <div class="buttonBar left">
                <legend>Dev Links</legend>
                    <a href="/Hex-Pawn/home.php">home</a> 
                    <a href="/Hex-Pawn/login.php">login</a>
                    <a href="/Hex-Pawn/gamePage.php">Games</a>
                </div> 
            </div>
            <div class="container">
                <div id="hexGrid" class="left">
                    <label id="hex"></label>
                    <script> DrawHexGrid(); </script>
                </div>
                <div id="hexInfo" class="left" style="width:400px;">
                    <label id="hexOptions"><h3 align="center">Select a hex to see its options.</h3></label>
                </div>
            </div>
        </div>
        <h6 class="center">HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license</h6>
    </body>
</html>

/*HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license*/

CREATE DATABASE IF NOT EXISTS HEXGAME;

CREATE TABLE IF NOT EXISTS HEXGAME.MAP(
	Name varchar(255) PRIMARY KEY,
	Width int,
	Height int
);

CREATE TABLE IF NOT EXISTS HEXGAME.TERRAIN_TYPES(
	Type varchar(255) PRIMARY KEY,
	T_Description varchar(1000)
);

CREATE TABLE IF NOT EXISTS HEXGAME.ELEMENTS(
	Element varchar(255),
	Terrain_Type varchar(255),
	E_Description varchar(1000),
	PRIMARY KEY (Element, Terrain_Type),
	FOREIGN KEY (Terrain_Type) REFERENCES HEXGAME.TERRAIN_TYPES (Type)
);

CREATE TABLE IF NOT EXISTS HEXGAME.HEX(
	Coord_X int,
	Coord_Y int,
	Map_Name varchar(255),
	Alias varchar(255),
	Terrain_Type varchar(255),
	Element varchar (255),
	PRIMARY KEY (Coord_X, Coord_Y, Map_Name),
	FOREIGN KEY (Terrain_Type) REFERENCES HEXGAME.TERRAIN_TYPES (Type),
	FOREIGN KEY (Element) REFERENCES HEXGAME.ELEMENTS (Element),
	FOREIGN KEY (Map_Name) REFERENCES HEXGAME.MAP (Name)
);

CREATE TABLE IF NOT EXISTS HEXGAME.NOTES(
	Created DATE PRIMARY KEY,
	Coord_X int,
	Coord_Y int,
	Note varchar(1000),
	FOREIGN KEY (Coord_X, Coord_Y) REFERENCES HEXGAME.HEX (Coord_X, Coord_Y)
);




INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Desert', 'A desert is any sort of terrain that receives very little rainfall. It can be warm, temperate, or cold.\nThis section pertains mostly to warm and temperate deserts. In cold environments, a desert is usually tundra, which acts like another terrain category depending on the current season. During most of the year, a cold desert is covered in a layer of permafrost, creating hard, stable terrain (which is treated as plains). During the warm season, the permafrost thaws and turns the area into mud (which is treated as marsh).');
INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Forest', 'A normal forest hex can be any sort of common forest: sparse patches of trees in the lowlands, thickly needled pines of the taiga, a lush tropical jungle, or even an ancient fruit tree grove turned overgrown and wild.');
INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Hill', 'A hill is lower and less steep than a mountain. Hills are often transitional terrain between mountains and plains.');
INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Marsh', 'Marshes, swamps, and bogs are challenging ground to traverse. Survival check DCs to avoid getting lost increase by 1 in a marsh hex.');
INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Mountain', 'Mountains form long barriers across the landscape that greatly impede the movement of travelers.');
INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Plain', 'Plains can be fields of high grasses, permanently frozen tundra, or flat badlands.');
INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Settlement', 'Normal settlement hexes are small villages or military encampments. Settlements usually appear with another terrain type they\'re built upon. Frequently used trails or even simple roads reduce travel time through the hex by 25—50% depending on the terrain type for that hex.');
INSERT INTO HEXGAME.TERRAIN_TYPES VALUES ('Water', 'Whether a river, lake, or ocean, this type of hex is predominantly water. If the PCs lack swim speeds or boats, it is best to treat lakes and oceans as obstacles for the PCs to travel around rather than through. Treat the shores of the water hex as the adjacent terrain type.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Desert', 'A difficult desert is a treacherous place, full of sand dunes, sinkholes, rubble, sandstorms, or numerous ravines. Rare seasonal rains might cause flash floods, sweeping away or drowning any creature in their path. Survival checks to avoid getting lost or to become un-lost in this hex gain a +1 bonus. Survival checks to get along in the wild increase by 5.\nTreat a flash flood as an avalanche, except instead of suffocating from being buried under rock, creatures who are buried must hold their breaths or start drowning.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Desert', 'A desert hex feature might be a city or tomb long buried under the sands, one or more geoglyphs, an unusual mesa, a majestic canyon, a tar pit, or an oasis. A tall structure—such as a mesa or ruined tower—can be used as a landmark for navigation or an observation point to get a better view of the surrounding area. Other features might point to hidden treasures, ley lines, or celestial conjunctions.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Desert', 'The hex might be home to one or more kinds of flying predators (typically dragons and sphinxes), poisonous monsters capable of tracking wounded prey over long distances, or subterranean creatures that use burrowing and similar tactics to make ambush attacks. The desert might also be home to nomadic raiders, genies, or elementals of a type fitting the desert\'s environment. The chance of random encounters within these deserts increases by 10%.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Desert', 'This hex might contain valuable ore, water (such as an oasis), or a rare but useful plant (such as a cactus used for medicine or exotic beverages).');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Desert', 'A secret desert hex might have shifting sand dunes, acrid winds, poisonous terrain, elemental portals, or some other strange feature that hides its secrets.\nRuins half-buried in the desert could still contain lost treasures or might already be looted. In either case, the ruins can be used as a place to take shelter from storms or as a lair for monsters. PCs who take shelter in these ruins suffer no effect from storms and similar hazards, but the chance of random encounters increases by 25%.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Forest', 'A difficult forest is a treacherous place, full of rotting trees that can fall without warning, twisted scythe trees that lunge at their victims, or witch-lights that lead expeditions off the path. For each hour spent traveling through a difficult forest, there is a 5% chance of a falling tree hazard. Survival check DCs to avoid getting lost increase by 5.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Forest', 'A forest hex feature could be either a cluster of massive old-growth trees or some type of tree that is unique to that region. In an old-growth forest, the canopy limits how much light reaches the ground, so undergrowth tends to be low-lying, tough plants like mosses and ferns.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Forest', 'This kind of forest hex is often treated with awe by local people, as hunting grounds are full of a terrifying array of arboreal creatures. The chance of random encounters within these forests increases by 10%.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Forest', 'This hex contains valuable lumber, medicinal herbs, or plentiful sources of game meat. Survival checks to get along in the wild gain a +5 bonus.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Forest', 'A secret forest hex has thick mists or deep shadows that make fully exploring it a time-consuming prospect. Exploration time increases by 50%.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Hill', 'Full of short cliffs and jagged stones, a difficult hill hex requires extra caution to avoid dangerous falls. For the purposes of travel and exploration times, treat the party\'s speed as one category slower.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Hill', 'The hex might be the site of a famous historical battle or the burial mound of long-dead chieftains. It provides a commanding view of the surrounding region and is useful as a waypoint. Survival checks to avoid getting lost or to become un-lost in this hex gain a +1 bonus.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Hill', 'The hex is cut with valleys and trenches that obscure predators from view. The chance of random encounters increases by 25%.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Hill', 'The hex contains resources such as quality stone, coal, precious metals, or gems.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Hill', 'Hidden caverns provide shelter and lairs for monsters. Locating these caverns requires a successful DC 10 Perception or Survival check. PCs who take shelter here suffer no effect from storms and similar hazards, but the chance of random encounters increases by 10%.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Marsh', ' A difficult marsh hex is a deadly place, replete with quicksand, poisonous plants, and treacherous water. The DCs for Survival checks to get along in the wild increase by 5. For the purposes of travel and exploration times, treat the party\'s speed as one category slower.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Marsh', 'The hex might be the location of a marsh creature\'s den (such as a hag), a sunken ruin, a large water causeway, or a shallow lake.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Marsh', 'Attacks in this hex are equally likely to come from underwater as from the surface. The chance of random encounters increases by 25%.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Marsh', 'Marsh resources primarily come in the form of medicinal plants and herbs.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Marsh', 'Unfortunate explorers died in the marsh and left behind all their gear. With a successful DC 25 Survival check, the PCs can each salvage equipment worth 10 gp per character level.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Mountain', 'All Climb DCs in a difficult mountain hex increase by 2. For the purposes of travel and exploration times, treat the party\'s speed as one category slower.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Mountain', 'The mountain is the highest in the vicinity or has an unusual shape, perhaps resembling a face or creature. Alternatively, use a feature from the Feature section of the hill hex terrain type.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Mountain', 'Bandits and monsters frequent these hexes, falling upon weary travelers. The chance of random encounters increases by 10%.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Mountain', 'The hex contains resources such as quality stone, coal, precious metals, or gems.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Mountain', 'Hidden pathways carved through the mountains offer speedier paths. If the PCs succeed at a DC 20 Perception check to find the pathways, they can ignore the default travel time increase for the mountain hex.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Plain', 'Dangerous plains tend to be filled with small sinkholes and pits that can twist or break the legs of the unwary. For the purposes of travel and exploration times, treat the party\'s speed as one category slower.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Plain', 'The plain might be the site of an old battlefield, with the remnants of earthwork defenses and trenches.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Plain', 'Ambush predators abound in these plains hexes, using the cover of tall grass to outflank and strike surprised prey. In tundra and badlands terrain, predators lie in wait underground using abilities such as burrow, or by digging shallow pits to hide in. The chance of random encounters increases by 25%.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Plain', 'The hex has edible plants (such as wheat or cacti) or useful vegetable matter (such as flax or cotton).');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Plain', 'Stolen goods are buried in the hex and marked with an innocuous sign, such as an out-of-place river rock. With a successful DC 25 Perception check, the PCs recognize the marker and can each salvage treasure or nonmagical gear worth 10 gp per character level.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Settlement', 'A difficult settlement hex holds the ruins of an abandoned town or one full of the victims of famine, plague, or another devastating event. Decrepit buildings might collapse at any moment (treat as a cave-in or collapse).');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Settlement', 'The settlement hex has a community with a well-known reputation or historical significance.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Settlement', 'This settlement is lawless, frequently attacked by brigands or pirates, or plagued by civil unrest. The chance of random encounters increases by 25%.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Settlement', ' The settlement is a trading post, merchant camp, or small fort on a crucial crossroad or river crossing, and goods of many types (particularly trade goods and natural resources from nearby hexes) pass through the area.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Settlement', ' A secret settlement is a bandit fort, pirate town, village inhabited by monsters, or secret home of someone trying to avoid normal civilization. The hex primarily resembles an adjacent hex type, and access to the settlement is usually hidden.');

INSERT INTO HEXGAME.ELEMENTS VALUES ('Difficult', 'Water', 'Whitewater rapids, strong tides, or underwater vortexes mean this water is more challenging to cross. The Swim DCs to cross these waters increase by 5.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Feature', 'Water', 'The hex is part of a large or well-known river\'s course, or has a sturdy bridge that facilitates easy crossing.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Hunting Ground', 'Water', 'The hex might be home to predatory aquatic creatures or opportunistic hunters waiting to strike prey that comes to drink. The chance of random encounters increases by 10%, or 25% if the PCs spend most of their time in the water.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Resource', 'Water', 'Fish, shellfish, and pearls are plentiful in the hex. In some situations, the benefit of this resource is the availability of fresh water rather than the contaminated water or salt water available in nearby hexes.');
INSERT INTO HEXGAME.ELEMENTS VALUES ('Secret', 'Water', 'The hex might contain an oasis, a connection to the Plane of Water, or a spring with magical powers.');
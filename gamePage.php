<!-- HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license -->

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Home - HexPawn</title>
        <script src="jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <div id="wrapper">
            <div class="container">
                <h2 class="center">Game Management</h2>
                <div class="column">
                <h3>GM Features</h3>
                    <div class="buttonBar left panel">
                    <legend>GM maps</legend>
                        +Add Map<br>
                        <hr>
                        map1<br>
                        map2<br>
                    </div>
                    <div class="buttonBar left panel">
                    <legend>Players</legend>
                        +Invite player<br>
                        <hr>
                        player1<br>
                        player2<br>
                    </div>
                </div>
                <div class="column">
                <h3>Player Features</h3>
                    <div class="buttonBar left panel">
                    <legend>Player maps</legend>
                        map1<br>
                        map2<br>
                    </div>
                    <div class="buttonBar left panel">
                    <legend>Army Builder</legend>
                        +Add Unit<br>
                        <hr>
                        unit1<br>
                        unit2<br>
                    </div>
                    <div class="buttonBar left panel">
                    <legend>Civ Builder</legend>
                        +Add Building<br>
                        <hr>
                        builidng1<br>
                        builidng2<br>
                    </div>
                    <div class="buttonBar left panel">
                    <legend>Tech Tree</legend>
                        +Add Tech<br>
                        <hr>
                        tech1<br>
                        tech2<br>
                    </div>
                </div>
            </div>
        </div>
        <h6 class="center">HexPawn is created by Lee Jones (Moosebiter) under a GNU v2 license</h6>
    </body>
</html>
